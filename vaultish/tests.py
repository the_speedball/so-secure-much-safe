from django.test import TestCase
from django.urls import reverse
from django.core.files import File
from freezegun import freeze_time
from rest_framework import status
from rest_framework.test import APITestCase
from vaultish.models import Element


class ElementModelTests(TestCase):
    def test_object_creation(self):
        el = Element.objects.create()

        assert len(el.password) == 32

    def test_object_expired(self):
        with freeze_time("2018-11-30"):
            el = Element.objects.create()

        assert el.is_expired()

    def test_element_not_expired(self):
        el = Element.objects.create()
        assert not el.is_expired()


class PostElementTests(APITestCase):
    def setUp(self):
        from django.contrib.auth import get_user_model

        user_model = get_user_model()
        user = user_model.objects.create()
        user.save()
        self.client.force_authenticate(user=user)

    def test_empty_post(self):
        url = reverse("element-add")
        response = self.client.post(url, {}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_failed_on_post_link_and_file(self):
        url = reverse("element-add")
        with open("./vaultish/serializers.py") as test_file:
            response = self.client.post(
                url, {"upl_link": "http://michalklich.com", "upl_file": test_file}
            )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_successful_post_link(self):
        url = reverse("element-add")
        response = self.client.post(
            url, {"upl_link": "http://michalklich.com"}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_successful_post_file(self):
        url = reverse("element-add")
        with open("./vaultish/serializers.py") as test_file:
            response = self.client.post(url, {"upl_file": test_file})

        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DetailElementTests(APITestCase):
    def test_invalid_password(self):
        el = Element.objects.create(upl_link="https://michalklich.com")
        url = reverse("element-get", args=[el.uuid])
        response = self.client.post(url, {"password": "not valid, mate"}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_expired_link(self):
        with freeze_time("1999-01-01"):
            el = Element.objects.create(upl_link="https://michalklich.com")

        url = reverse("element-get", args=[el.uuid])
        response = self.client.post(url, {"password": el.password}, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_successful_request_url(self):
        el = Element.objects.create(upl_link="https://michalklich.com")
        url = reverse("element-get", args=[el.uuid])
        response = self.client.post(url, {"password": el.password}, format="json")

        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertTrue(response.has_header("Location"))
        self.assertEqual(response["Location"], "https://michalklich.com")

    def test_successful_request_file(self):
        with open("./vaultish/serializers.py") as test_file:
            el = Element.objects.create(upl_file=File(test_file))

        url = reverse("element-get", args=[el.uuid])
        response = self.client.post(url, {"password": el.password})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(response.has_header("Content-Disposition"))
