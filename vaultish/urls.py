from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static

from . import views

urlpatterns = [
    url(r"^stats/", views.Stats.as_view()),
    url(r"^element_add/", views.ElementAdd.as_view(), name="element-add"),
    url(
        r"^e/(?P<element_uuid>[0-9a-z]{8}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{4}-[0-9a-z]{12})/",
        views.ElementDetails.as_view(),
        name="element-get",
    ),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
