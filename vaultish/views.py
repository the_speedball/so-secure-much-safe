from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect
from rest_framework import status
from rest_framework.generics import CreateAPIView
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from vaultish.models import Element

from .serializers import CerberusSerializer, ElementSerializer


class ElementAdd(CreateAPIView):
    serializer_class = ElementSerializer
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]
    template_name = "upload.html"
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        # FIXME: fails with json renderer
        return Response({"serializer": ElementSerializer()})

    def post(self, request):
        serializer = ElementSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        element = serializer.save()
        return Response(
            status=status.HTTP_200_OK,
            template_name="success.html",
            data={
                "uuid": element.uuid,
                "expiration": element.expiration,
                "password": element.password,
            },
        )


class ElementDetails(APIView):
    template_name = "cerberus.html"
    serializer_class = CerberusSerializer
    renderer_classes = [TemplateHTMLRenderer, JSONRenderer]

    def get(self, request, element_uuid):
        # TODO: possibly move this test to other place
        get_object_or_404(Element, uuid=element_uuid)

        return Response({"serializer": CerberusSerializer})

    def post(self, request, element_uuid):
        el = get_object_or_404(Element, uuid=element_uuid)

        serializer = CerberusSerializer(
            data=request.data, context={"element_uuid": element_uuid}
        )
        serializer.is_valid(raise_exception=True)

        el.count += 1
        el.save()

        if el.upl_link:
            return redirect(el.upl_link)

        response = HttpResponse(el.upl_file)
        response["Content-Disposition"] = f"attachment; filename={el.upl_file.name}"
        return response


class Stats(APIView):
    renderer_classes = [JSONRenderer]
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        visited_elements = Element.objects.filter(count__gt=0).values_list(
            "created_at", "upl_link", "upl_file"
        )

        def _sorter(x, y):
            if x:
                return "links"
            return "files"

        frmt = [
            (e[0].strftime("%Y-%m-%d"), _sorter(e[1], e[2])) for e in visited_elements
        ]

        d_stats = {}

        for date_, type_ in frmt:
            day_ = d_stats.get(date_, {"files": 0, "links": 0})
            day_[type_] += 1
            d_stats[date_] = day_

        return Response(data=d_stats, status=status.HTTP_200_OK)
