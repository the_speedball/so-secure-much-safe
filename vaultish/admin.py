from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from vaultish.models import TrackedUser, Element


class UserAdmin(BaseUserAdmin):
    list_display = (
        "username",
        "email",
        "first_name",
        "last_name",
        "is_staff",
        "user_agent",
    )
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            "Personal info",
            {"fields": ("first_name", "last_name", "email", "user_agent")},
        ),
        ("Permissions", {"fields": ("is_active", "is_staff", "is_superuser")}),
    )


admin.site.register(Element)
admin.site.register(TrackedUser, UserAdmin)
