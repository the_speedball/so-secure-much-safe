class UATrackingMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        return self.get_response(request)

    def process_view(self, request, view_func, view_args, view_kwargs):
        if request.user.is_authenticated():
            ua = request.META.get("HTTP_USER_AGENT", "unknown")
            request.user.user_agent = ua
            request.user.save()
