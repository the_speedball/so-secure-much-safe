from django.apps import AppConfig


class VaultishConfig(AppConfig):
    name = "vaultish"
