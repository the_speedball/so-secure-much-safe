import datetime
from rest_framework import serializers

from .models import Element


class CerberusSerializer(serializers.Serializer):
    password = serializers.CharField(max_length=32, required=True)

    def validate_password(self, value):
        el = Element.objects.get(uuid=self.context["element_uuid"])
        if el.password != value:
            raise serializers.ValidationError("None shall pass")

    def validate(self, data):
        el = Element.objects.get(uuid=self.context["element_uuid"])

        if el.is_expired():
            raise serializers.ValidationError("Link expired.")

        return data


class ElementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Element
        fields = ("upl_file", "upl_link")

    def validate(self, data):
        """
        Check that start is before finish.
        """
        no_file_nor_link = not (data.get("upl_file") or data.get("upl_link"))
        if no_file_nor_link:
            raise serializers.ValidationError("Please provide either a file or a link.")

        file_and_link = data.get("upl_file") and data.get("upl_link")
        if file_and_link:
            raise serializers.ValidationError("Please send either link or file.")

        return data
