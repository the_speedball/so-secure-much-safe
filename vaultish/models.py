import datetime
import random
import string
import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models


def _passgen():
    return "".join((random.choice(string.ascii_letters) for _ in range(1, 33)))


def _expiration():
    return datetime.datetime.now(datetime.timezone.utc) + datetime.timedelta(days=1)


class TrackedUser(AbstractUser):
    user_agent = models.TextField()


class Element(models.Model):
    upl_file = models.FileField(blank=True, upload_to="uploads/")
    upl_link = models.URLField(blank=True)

    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    link = models.CharField(max_length=200, blank=True)
    count = models.BigIntegerField(default=0)
    password = models.CharField(max_length=32, default=_passgen)
    expiration = models.DateTimeField(default=_expiration)
    created_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return f"link: {self.link} - {self.created_at} - {self.upl_link}"

    def is_expired(self):
        return (
            datetime.datetime.now(datetime.timezone.utc) - self.expiration
        ) > datetime.timedelta(days=1)

    def save(self, *args, **kwargs):
        self.link = f"/e/{self.uuid}"
        super().save(*args, **kwargs)
